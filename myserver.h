#ifndef MYSERVER_H
#define MYSERVER_H

#include "mainwindow.h"

class QTcpServer;
class QTextEdit;
class QTcpSocket;

class MyServer : public QWidget {
    Q_OBJECT
private:
    QTcpServer* m_ptcpServer; // атрибут m_ptctServer является основой управления сервером
    QTextEdit* m_ptxt; // многострочное текстовое плое, для информации о происходящих событиях
    quint16 m_nNextBlockSize; // служит для хранения длины следующего полученного от сокета блока

private:
    void sendToClient(QTcpSocket* pSocket, const QString& str);

public:
    MyServer(int nPort, QWidget* pwgt=0);

public slots:
    virtual void slotNewConnection();
    void slotReadClient();
};

#endif // MYSERVER_H
